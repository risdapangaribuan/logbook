<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'GuestController@index');
Route::get('/guests/create', 'GuestController@create');
Route::post('/guests', 'GuestController@store');
Route::post('/guests', 'GuestController@store');
Route::get('/guests/{id}/edit', 'GuestController@edit');
Route::patch('/guests/{id}', 'GuestController@update');
Route::delete('/guests/{id}', 'GuestController@destroy');
