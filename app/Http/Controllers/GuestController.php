<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Guest;

class GuestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $guests = DB::table('guests')->paginate(5);
       // return view('layouts.index',['guests' => $guests]);
        return view('layouts.index',compact('guests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('layouts.create-guest');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasFile('foto')){
        $foto = $request->foto->getClientOriginalName() . '_' . time()
                                 . '.' . $request->foto->extension();
          $request->foto->move(public_path('image'), $foto);
         } else {
                $foto = null;
              }
        $guests = new Guest([
            'name' => $request->input('name'),
            'telephone' => $request->input('telephone'),
            'company' => $request->input('company'),
            'email' => $request->input('email'),
            'activity' => $request->input('activity'),
            'name' => $request->input('name'),
            'noRack' => $request->input('noRack'),
            'noLoker' => $request->input('noLoker'),
            'foto' => $foto
        ]);
        $guests->save();
        return redirect('/')->with('message','Data Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $guest = Guest::find($id);
        return view('layouts.edit-guest', compact('guest'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // if($request->file('foto')) {
        //     $foto = $request->foto->getClientOriginalName() . '_' . time()
        //                          . '.' . $request->foto->extension();
        //   $request->foto->move(public_path('image'), $foto);

        //     $guest = Guest::findOrfail($id);
        //     if ($guest->foto){
        //      Storage::delete('image/' .$guest->foto);
        //      $guest->foto = $foto;
        //     } else {
        //         $guest->foto =$foto;
        //     }
        //         $guest->save();
        // }

            $ubah = Guest::findorfail($id);
            $awal = $ubah->foto;
            
            $guest = [
                'name' => $request['name'],
                'telephone' => $request['telephone'],
                'company' => $request['company'],
                'email' => $request['email'],
                'activity' => $request['activity'],
                'noRack' => $request['noRack'],
                'noLoker' => $request['noLoker'],
                'foto'=>$awal,
            ];
            $request->foto->move(public_path().'/image',$awal);
            $ubah->update($guest);



        // $guest = Guest::find($id);
        // $guest->name = $request->input('name');
        // $guest->telephone = $request->input('telephone');
        // $guest->company = $request->input('company');
        // $guest->email = $request->input('email');
        // $guest->activity = $request->input('activity');
        // $guest->noRack = $request->input('noRack');
        // $guest->noLoker = $request->input('noLoker');
        // // $guest->foto = $request->input('foto');
        // $guest->save();
        return redirect('/')->with('message','Data Berhasil Diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $guest = Guest::findorfail($id);
        $foto= public_path('/image/').$guest->foto;
        if (file_exists($foto)){
            @unlink($foto);
        }
        $guest->delete();

        return redirect('/')->with('message','Data Berhasil Dihapus');
    }
}
