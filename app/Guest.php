<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\facades\DB;

class Guest extends Model
{
    public $timestamps = false;
    protected $fillable = ['name','telephone', 'company', 'email', 'activity', 'noRack', 'noLoker', 'foto'];
}
