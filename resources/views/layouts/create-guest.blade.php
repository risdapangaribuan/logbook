@extends('master')
@section('title','Guest Book Nap Info')
@section('content') 
<div class="section-body"> 
    <form action="{{ url('/guests')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="card-body-center">
                <h4> Tambah Data Guest </h4>
                <div class="form-row justify-content-center">  
                    <div class="form-group col-md-6">
                        <label>Name </label>
                        <input type="text" class="form-control"  placeholder="Name" name="name" >
                    </div>
                    <div class="form-group col-md-6">
                        <label>Telephone</label>
                        <input type="text" class="form-control"  placeholder="Telephone" name="telephone"  >
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Company </label>
                        <input type="text" class="form-control"  placeholder="Company" name="company" >
                    </div>
                    <div class="form-group col-md-6">
                        <label>Email </label>
                        <input type="text" class="form-control"  placeholder="Email" name="email" >
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Activity</label>
                        <input type="text" class="form-control"  placeholder="Activity" name="activity" >
                    </div>
                    <div class="form-group col-md-6">
                        <label >No Rack </label>
                        <input type="text" class="form-control"  placeholder="No Rack" name="noRack" >
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label >No Loker</label>
                        <input type="text" class="form-control"  placeholder="No Loker" name="noLoker" >
                    </div>
                    <div class="form-group col-md-6">
                        <label>Foto :</label>
                        <input type="file" class="form-control-file" name="foto"  id="foto">
                    </div>
                </div>
                <div class="card-footer text-center" color="none">
                    <button class="btn btn-primary mr-1" type="submit"> Submit </button>
                    <button class="btn btn-secondary" type="reset"> Reset </button>
                    <a class="btn btn-outline-info" href="{{ url('/')}}" > Back </a>
                </div>
            </div>
        </div>
    </form>
    </div>
@endsection
@push('page-scripts')
@endpush
