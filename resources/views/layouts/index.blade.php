@extends('master')
@section('title','Guest Book Nap Info')
@section('content') 
            <div>
            <a class="btn btn-icon icon-left btn-info" href="{{url('guests/create')}}"><i class="fa fa-plus" aria-hidden="true"></i> Tambah Guest</a>
            <hr>
            @if(session('message'))
                <div class="alert alert-success alert-dismissible show fade">
                    <div class="alert-body">
                        <button class="close" data-dismiss="alert">
                            <span>&times;</span>
                        </button>
                        {{session('message')}}
                    </div>
                </div>
            @endif
                           <table class="table table-hover table-striped table-sm" >
                <thead class="thead-dark"> 
                <tr>
                    <th scope="col">No </th>
                    <th scope="col">Nama </th>
                    <th scope="col">Telephone </th>
                    <th scope="col">Company </th>
                    <th scope="col">Email</th>
                    <th scope="col">Activity</th>
                    <th scope="col">No Rack </th>
                    <th scope="col">No Loker</th>
                    <th scope="col">Foto </th>
                    <th colspan="2">Action</th>
                </tr>
            </thead>
                    
            @foreach ($guests as  $no => $guest)
                    <tr>
                       
                    <td>{{ $guests->firstItem()+$no}} </td> 
                    <td>{{$guest->name}}</td>
                    <td>{{$guest->telephone}}</td>
                    <td>{{$guest->company}}</td>
                    <td>{{$guest->email}}</td>
                    <td>{{$guest->activity}}</td>
                    <td>{{$guest->noRack}}</td>
                    <td>{{$guest->noLoker}}</td>
                    <td> 
                        @if($guest->foto)
                        <img src="/image/{{$guest->foto}}" width="60" height="60" alt="">
                        @else
                           <i>NULL</i>
                        @endif
                    </td>
                    <td> 
                        <a href="{{ url("guests/{$guest->id}/edit") }}" class="btn btn-outline-info"><i class="fa fa-edit"></i></a>
                    </td>  
                    <td>
                    <form action="{{ url("guests/{$guest->id}") }}" id="#" method="POST" onsubmit="return confirm('Yakin Hapus Data?')">
                            @csrf
                            @method('delete')
                            <button class="btn btn-outline-danger">
                                <i class="fa fa-trash"></i>
                        </button> 
                    </form>
                </td>
                @endforeach
                </tr>  
            </table>
       {{$guests->links()}}
        </div>
@endsection
    @push('page-scripts')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

   @endpush

   @push('after-scripts')
   
  @endpush